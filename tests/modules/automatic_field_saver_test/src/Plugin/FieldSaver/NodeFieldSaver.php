<?php

namespace Drupal\automatic_field_saver_test\Plugin\FieldSaver;

use Drupal\automatic_field_saver\FieldSaver\FieldSaverPluginBase;
use Drupal\Core\Entity\EntityInterface;

/**
 * Class NodeFieldSaver.
 *
 * @FieldSaver(
 *   id="node_field_saver",
 *   entityType="node",
 *   bundle="test_node_type",
 *   fields={"body"}
 * )
 */
class NodeFieldSaver extends FieldSaverPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getFieldValue(EntityInterface $entity) {
    $body_value = $entity->get('body')->getValue();
    if (empty($body_value)) {
      return $entity->label();
    }
    return $body_value;
  }

}
