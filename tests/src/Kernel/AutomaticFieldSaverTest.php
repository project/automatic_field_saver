<?php

namespace Drupal\Tests\automatic_field_saver\Kernel;

use Drupal\KernelTests\KernelTestBase;
use Drupal\node\Entity\NodeType;
use Drupal\node\Entity\Node;
use Drupal\user\Entity\User;

/**
 * Class AutomaticFieldSaverTest.
 *
 * @group automatic_field_saver
 */
class AutomaticFieldSaverTest extends KernelTestBase {

  /**
   * The modules that will be enabled during testing.
   *
   * @var array
   */
  public static $modules = [
    'system',
    'field',
    'text',
    'user',
    'node',
    'language',
    'automatic_field_saver_test',
    'automatic_field_saver',
  ];

  /**
   * The machine name for the test node type.
   */
  const NODE_TYPE_NAME = 'test_node_type';

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();

    $this->installEntitySchema('node');
    $this->installEntitySchema('user');
    $this->installConfig(['field', 'node', 'user']);

    // Create a new content type.
    $content_type = NodeType::create([
      'type' => static::NODE_TYPE_NAME,
      'name' => 'Test node type',
    ]);
    $content_type->save();
    // Add a body field.
    node_add_body_field($content_type);
  }

  /**
   * Verifies that the values are correctly saved.
   */
  public function testSaver() {
    // Creates the node, with no body.
    $node1 = Node::create([
      'type'        => static::NODE_TYPE_NAME,
      'title'       => 'Title test',
    ]);
    $node1->save();
    // After save the value for the body field must be the same as the title.
    $body1 = $node1->get('body')->getValue()[0]['value'];
    self::assertEquals('Title test', $body1, 'Body equals title.');
    // Created the node, with a body set.
    $node2 = Node::create([
      'type' => static::NODE_TYPE_NAME,
      'title' => 'Title test2',
      'body' => 'New body text.'
    ]);
    $node2->save();
    // After save, the value for the body must not be overridden.
    $body2 = $node2->get('body')->getValue()[0]['value'];
    self::assertNotEquals('Title test2', $body2, 'Body is different than the title.');
  }

}
