# Introduction
Sets fields on entities based on certain rules.

# Use case
If you want to automatize the saving of values on specific fields, based on
specific conditions. This is an object oriented approach to the classic entity
presave hook. All logic will be done in the plugin.

# Install
First you would need to install the module. In order to do this, you can either
download the archive or use composer:
```composer require drupal/automatic_field_saver```

# Usage example
After installing the project. create a plugin as in this example:

``` PHP
/**
 * Class NodeFieldSaver.
 *
 * @FieldSaver(
 *   id="node_field_saver",
 *   entityType="node",
 *   bundle="test_node_type",
 *   fields={"body"}
 * )
 */
class NodeFieldSaver extends FieldSaverPluginBase {

  /**
   * {@inheritdoc}
   */
  public function getFieldValue(EntityInterface $entity) {
    $body_value = $entity->get('body')->getValue();
    // Create some logic for returning the value.
    if (empty($body_value)) {
      return $entity->label();
    }
    return $body_value;
  }

}
```
This way, every time a node of type test_node_type will be saved, the field
"body" will be set with the returned value of the method
getFieldValue().
